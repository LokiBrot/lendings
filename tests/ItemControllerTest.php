<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 03.02.17
 * Time: 16:00
 */


use Faker\Generator;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Lendings\Item;
use Lendings\ItemType;


class ItemControllerTest extends TestCase
{
    use DatabaseMigrations, InteractsWithDatabase;

    /** @var Generator */
    protected $faker = null;

    public function setUp()
    {
        parent::setUp();

        $this->faker = app(Generator::class);
    }

    public function testCreatesItemFromCorrectRequest()
    {
        /** @var ItemType $itemType */
        $itemType = factory(ItemType::class)->create();

        $datafields = [];

        foreach (range(0, 9) as $_) {
            $datafields[] = [
                'name'  => $this->faker->word,
                'value' => $this->faker->word,
                'type'  => $this->faker->word,
            ];
        }

        $itemData = [
            'name'        => $this->faker->word,
            'itemType'    => $itemType->id,
            'description' => $this->faker->text,
            'datafields'  => $datafields,
        ];

        $resp = $this->post('/api/items', $itemData);

        /** @var Item $item */
        $item = Item::where('name', $itemData['name'])->first();
        $resp->assertStatus(200);
        $resp->assertJson($item->toArray());
        $this->assertDatabaseHas('items', ['name' => $itemData['name']]);
    }

    public function /*test*/FailsOnIncorrectRequest()
    {
        $resp = $this->post('/api/items', [
            'nonsense' => 666
        ]);

        dd($resp->getContent());
    }

}
