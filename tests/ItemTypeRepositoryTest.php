<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 03.02.17
 * Time: 19:55
 */


use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Lendings\Contracts\ItemTypeRepositoryContract;
use Lendings\ItemType;
use Lendings\Repositories\ItemTypeRepository;


class ItemTypeRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    /** @var  ItemTypeRepository */
    protected $itemTypes = null;

    /** @var Collection */
    protected $dbItemTypes = null;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->itemTypes = app(ItemTypeRepositoryContract::class);

        $this->dbItemTypes = factory(ItemType::class, 10)->create();
    }

    public function testRetrievesASingleItemType()
    {
        /** @var ItemType $itemType */
        $itemType = $this->dbItemTypes->random();

        /** @var ItemType $retrieved */
        $retrieved = $this->itemTypes->get($itemType->id);

        $this->assertEquals($itemType->id, $retrieved->id, 'Failed retrieving a single ItemType by integer id.');

        $retrieved = $this->itemTypes->get((string)$itemType->id);

        $this->assertEquals($itemType->id, $retrieved->id, 'Failed retrieving a single ItemType by string id.');
    }

    public function testRetrievesMultipleItemTypes()
    {
        /** @var Collection $itemTypes */
        $itemTypes = $this->dbItemTypes->random(5);

        $itemTypeKeys = $itemTypes->map(function (ItemType $itemType) {
            return $itemType->id;
        });


        /** @var Collection $retrieved */
        $retrieved = $this->itemTypes->get($itemTypeKeys->toArray());

        $itemTypeKeys = array_flip($itemTypeKeys->toArray());
        $retrieved->each(function (ItemType $itemType) use ($itemTypeKeys) {
            $this->assertTrue(array_key_exists($itemType->id, $itemTypeKeys));
        });
    }
}
