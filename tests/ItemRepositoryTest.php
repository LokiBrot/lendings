<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 03.02.17
 * Time: 15:02
 */


use Faker\Generator;
use Illuminate\Foundation\Testing\Concerns\InteractsWithDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Lendings\ItemType;
use Lendings\Repositories\ItemRepository;


class ItemRepositoryTest extends TestCase
{
    use DatabaseMigrations, InteractsWithDatabase;

    /**
     * @var ItemRepository
     */
    protected $itemRepository = null;

    /**
     * @var Generator
     */
    protected $faker = null;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->itemRepository = app(ItemRepository::class);
        $this->faker = app(Generator::class);
    }

    public function testCreatesItemWithDatafields()
    {
        $itemTypes = factory(ItemType::class, 5)->create();

        $datafields = [];

        foreach (range(0, 9) as $_) {
            $datafields[] = [
                'name' => $this->faker->word,
                'value' => $this->faker->word,
                'type' => $this->faker->word,
            ];
        }

        $data = [
            'name' => $this->faker->word,
            'itemType' => $itemTypes->random()->id,
            'description' => $this->faker->text,
            'datafields' => $datafields,
        ];

        $item = $this->itemRepository->createWithData($data);

        $this->assertDatabaseHas('items', $item->toArray());

        foreach ($datafields as $datafield) {
            $this->assertDatabaseHas('item_data_fields', $datafield);
        }
    }
}
