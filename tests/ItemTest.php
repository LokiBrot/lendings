<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Lendings\Item;

class ItemTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test if an Item Model can be created and saved to DB.
     *
     * @return void
     */
    public function testCanBeCreated()
    {
        $itemType = factory(\Lendings\ItemType::class)->create();

        /** @var Item $item */
        $item = factory(Item::class)->create([
            'item_type_id' => $itemType->id,
        ]);

        $this->assertSame($item->item_type_id, $item->type->id);
    }
}
