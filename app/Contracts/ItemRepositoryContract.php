<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 03.02.17
 * Time: 19:36
 */
namespace Lendings\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use Lendings\Item;

interface ItemRepositoryContract
{
    /**
     * @param array $data
     * @return Item
     */
    public function createWithData(array $data): Item;

    public function all(int $perPage = null, array $columns = array('*'), string $pageName = 'page', int $page = null): LengthAwarePaginator;
}