<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 03.02.17
 * Time: 19:57
 */
namespace Lendings\Contracts;

use Illuminate\Database\Eloquent\Collection;


/**
 * Class ItemTypeRepository
 *
 * @package Lendings\Repositories
 */
interface ItemTypeRepositoryContract
{
    /**
     * Get one, multiple or all ItemTypes from the database.
     *
     * @param array $ids Numeric value to get one Model, array of integers to get multiple, empty to get all.
     *
     * @return Collection|null|static[] Collection if multiple, ItemType model directly if one. Null if not found.
     */
    public function get($ids = []);
}