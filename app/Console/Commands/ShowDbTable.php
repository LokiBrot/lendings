<?php

namespace Lendings\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;

class ShowDbTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:show-table
                            {table : The table to show entries from}
                            {limit? : Maximum entries to show. Optional.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show entries from a given table';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table = $this->argument('table');
        $limit = $this->argument('limit');

        $this->info("Table: $table");

        $entries = $this->getEntries($table, $limit);

        if ($entries->isEmpty()) {
            $this->comment("No entries in table '$table'.");
            return 0;
        }

        $this->tableOutput($entries);

        return 0;
    }

    /**
     * @param $table
     * @param $limit
     *
     * @return Collection
     */
    public function getEntries($table, $limit): Collection
    {
        /** @var Builder $builder */
        $builder = \DB::table($table);

        if (isset($limit)) {
            $this->info("Setting limit: $limit");
            $builder->limit($limit);
        }

        $entries = $builder->get();

        return $entries;
    }

    /**
     * @param Collection $entries
     */
    public function tableOutput(Collection $entries): void
    {
        $headers = array_keys(get_object_vars($entries->first()));

        $entries->transform(function ($val) {
            return array_values(get_object_vars($val));
        });

        $data = $entries->all();

        $this->table($headers, $data);
    }
}
