<?php

namespace Lendings\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Support\ServiceProvider;
use Lendings\Contracts\ItemRepositoryContract;
use Lendings\Contracts\ItemTypeRepositoryContract;
use Lendings\Repositories\ItemRepository;
use Lendings\Repositories\ItemTypeRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
        $this->app->singleton(ItemRepositoryContract::class, ItemRepository::class);
        $this->app->singleton(ItemTypeRepositoryContract::class, ItemTypeRepository::class);
    }
}
