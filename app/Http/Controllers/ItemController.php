<?php

namespace Lendings\Http\Controllers;

use Faker\Generator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Lendings\Contracts\ItemRepositoryContract;
use Lendings\Http\Requests\StoreItem;
use Lendings\Item;
use Lendings\Repositories\ItemRepository;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('items.index');
    }

    /**
     * Return a listing of the resource as json.
     *
     * @return Response
     */
    public function apiIndex(Request $request, ItemRepositoryContract $items)
    {

        return response()->json($items->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreItem $request
     * @param ItemRepository $items
     * @return Response
     */
    public function store(StoreItem $request, ItemRepository $items)
    {
        return response()->json($items->createWithData($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Generator $faker
     * @param $items
     */
    public function transformForApi(Generator $faker, $items): void
    {
        /** @var LengthAwarePaginator $items */
        $items =& $items->items();

        array_walk($items, function ($item) use ($faker) {
            /** @var Item $item */
            return [
                'url'         => '/items/' . $item->id,
                'imageUrl'    => $faker->imageUrl(64, 64, 'cats', true, 'Lendings'),
                'name'        => $item->name ?? '',
                'description' => $item->description ?? '',
            ];
        });
    }
}
