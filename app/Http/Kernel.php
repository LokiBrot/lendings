<?php

namespace Lendings\Http;

use Illuminate\{
    Foundation\Http\Kernel as HttpKernel,
    Auth\Middleware\Authorize,
    Auth\Middleware\Authenticate,
    Auth\Middleware\AuthenticateWithBasicAuth,
    Cookie\Middleware\AddQueuedCookiesToResponse,
    Foundation\Http\Middleware\CheckForMaintenanceMode,
    Routing\Middleware\SubstituteBindings,
    Routing\Middleware\ThrottleRequests,
    Session\Middleware\StartSession,
    View\Middleware\ShareErrorsFromSession
};
use Lendings\Http\Middleware\{
    EncryptCookies,
    VerifyCsrfToken,
    RedirectIfAuthenticated
};
use Laravel\Passport\Http\Middleware\CreateFreshApiToken;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            EncryptCookies::class,
            AddQueuedCookiesToResponse::class,
            StartSession::class,
            ShareErrorsFromSession::class,
            VerifyCsrfToken::class,
            SubstituteBindings::class,
            CreateFreshApiToken::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => Authenticate::class,
        'auth.basic' => AuthenticateWithBasicAuth::class,
        'bindings' => SubstituteBindings::class,
        'can' => Authorize::class,
        'guest' => RedirectIfAuthenticated::class,
        'throttle' => ThrottleRequests::class,
    ];
}
