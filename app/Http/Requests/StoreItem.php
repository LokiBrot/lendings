<?php

namespace Lendings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreItem extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'itemType'           => 'required|Integer',
            'name'               => 'required|String|max:255|unique:items',
            'description'        => 'present',
            'datafields'         => 'sometimes|Array',
            'datafields.*.name'  => 'required|String|max:255',
            'datafields.*.value' => 'required',
            'datafields.*.type'  => 'required',
        ];
    }
}
