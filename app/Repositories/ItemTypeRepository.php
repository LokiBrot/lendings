<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 03.02.17
 * Time: 19:39
 */

namespace Lendings\Repositories;


use Illuminate\Database\Eloquent\Collection;
use Lendings\Contracts\ItemTypeRepositoryContract;
use Lendings\ItemType;

/**
 * Class ItemTypeRepository
 *
 * @package Lendings\Repositories
 */
class ItemTypeRepository implements ItemTypeRepositoryContract
{
    /**
     * Get one, multiple or all ItemTypes from the database.
     *
     * @param array $ids Numeric value to get one Model, array of integers to get multiple, empty to get all.
     *
     * @return Collection|null|static[] Collection if multiple, ItemType model directly if one. Null if not found.
     */
    public function get($ids = [])
    {
        $itemTypes = null;
        if (!empty($ids)) {
            if (is_array($ids)) {
                $itemTypes = ItemType::findMany($ids);
            } elseif (is_numeric($ids)) {
                $itemTypes = ItemType::find((int)$ids);
            } else {
                throw new \UnexpectedValueException("\$ids has to be either an array or a numeric.");
            }
        } else {
            $itemTypes = ItemType::all();
        }

        return $itemTypes;
    }

}