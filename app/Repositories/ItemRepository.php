<?php
/**
 * Created by IntelliJ IDEA.
 * User: chensink_privat
 * Date: 01.02.17
 * Time: 12:09
 */

namespace Lendings\Repositories;


use Illuminate\Pagination\LengthAwarePaginator;
use Lendings\Contracts\ItemRepositoryContract;
use Lendings\Item;
use Lendings\ItemType;

class ItemRepository implements ItemRepositoryContract
{
    /**
     * @param array $data
     *
     * @return Item
     */
    public function createWithData(array $data): Item
    {
        /** @var ItemType $itemType */
        $itemType = ItemType::findOrFail($data['itemType']);
        /** @var Item $item */
        $item = new Item([
            'name'        => $data['name'],
            'description' => $data['description'],
        ]);

        $itemType->items()->save($item);
        $datafields = $data['datafields'];

        $item->_data()->createMany($datafields);

        $item->save();
        $itemType->save();

        return $item;
    }

    public function all(
        int $perPage = null,
        array $columns = ['*'],
        string $pageName = 'page',
        int $page = null
                                ): LengthAwarePaginator
    {
        return Item::paginate($perPage, $columns, $pageName, $page);
    }

}