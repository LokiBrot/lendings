<?php

namespace Lendings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ItemDataField
 *
 * @property int $id
 * @property string $name
 * @property mixed $value
 * @property string $type
 */
class ItemDataField extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'value',
        'type',
    ];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function getValueAttribute($value)
    {
        return json_decode($value);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = json_encode($value);

        return $this;
    }
}
