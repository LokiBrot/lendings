<?php

namespace Lendings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ItemType
 *
 * @property int $id
 * @property string $name
 * @property array $datafields
 *
 * @method static findOrFail(int $id)
 * @method static findMany(array $ids, array $columns = ['*'])
 * @method static find(int $id)
 */
class ItemType extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'datafields',
    ];

    protected $visible = [
        'id',
        'name',
        'datafields',
    ];

    protected $casts = [
        'datafields' => 'array',
    ];

    public function items()
    {
        return $this->hasMany(Item::class);
    }
}

