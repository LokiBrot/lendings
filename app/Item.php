<?php

namespace Lendings;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Item
 * @property int $id
 * @property ItemType $type
 * @property string $name
 * @property string $description
 * @property Collection $_data
 * @internal int $item_type_id
 *
 * @method static create(array $data)
 */
class Item extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
    ];

    protected $visible = [
        'id',
        'name',
        'description',
    ];

    protected $appends = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ItemType::class, 'item_type_id');
    }

    public function _data()
    {
        return $this->hasMany(ItemDataField::class);
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {
        $ret = parent::__get($key);

        if (isset($ret)) {
            return $ret;
        }

        $ret = $this->_data->where('name', $key);

        return $ret;
    }
}
