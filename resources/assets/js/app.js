
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('itemlist', require('./components/ItemList.vue'));
Vue.component('vuetable', require('vuetable-2/src/components/Vuetable.vue'));
Vue.component('vuetable-pagination', require('vuetable-2/src/components/VuetablePagination.vue'));

const app = new Vue({
    el: '#app',
});
