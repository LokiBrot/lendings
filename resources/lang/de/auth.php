<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Diese Nutzerdaten stimmen nicht mit unseren Daten überein.',
    'throttle' => 'Zu viele Loginversuche. Versuchen sie es in :seconds Sekunden noch einmal.',

];
