<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute muss akzeptiert werden.',
    'active_url'           => ':attribute ist keine gültige URL.',
    'after'                => ':attribute muss ein Datum nach :date sein.',
    'after_or_equal'       => ':attribute muss ein Datum gleich oder später als :date sein.',
    'alpha'                => ':attribute darf nur Buchstaben enthalten.',
    'alpha_dash'           => ':attribute darf nur Buchstaben, Zahlen und Bindestriche enthalten.',
    'alpha_num'            => ':attribute darf nur Buchstaben und Zahlen enthalten',
    'array'                => ':attribute muss ein Array sein.',
    'before'               => ':attribute muss ein Datum vor :date sein.',
    'before_or_equal'      => ':attribute muss ein Datum gleich oder früher als :date sein.',
    'between'              => [
        'numeric' => ':attribute muss zwischen :min lund :max liegen.',
        'file'    => 'Die Größe von :attribute muss zwischen :min und :max Kilobytes liegen.',
        'string'  => ':attribute muss zwischen :min und :max Zeichen enthalten.',
        'array'   => ':attribute muss zwischen :min und :max Elemente enthalten.',
    ],
    'boolean'              => ':attribute muss entweder true oder false sein.',
    'confirmed'            => ':attribute: Überprüfung stimmt nicht überein.',
    'date'                 => ':attribute ist kein gültiges Datum.',
    'date_format'          => ':attribute erfüllt nicht das Format :format.',
    'different'            => ':attribute und :other müssen verschieden sein.',
    'digits'               => ':attribute muss :digits Stellen haben.',
    'digits_between'       => ':attribute muss zwischen :min und :max Stellen haben.',
    'dimensions'           => ':attribute hat ungültige Bilddimensionen.',
    'distinct'             => ':attribute enthält eine Duplette.',
    'email'                => ':attribute muss eine gültige Emailadresse sein.',
    'exists'               => 'Auswahl für :attribute ist ungültig.',
    'file'                 => ':attribute muss eine Datei sein.',
    'filled'               => ':attribute muss ausgefüllt sein.',
    'image'                => ':attribute ein Bild sein.',
    'in'                   => 'Auswahl für :attribute ist ungültig.',
    'in_array'             => ':attribute muss in :other enthalten sein.',
    'integer'              => ':attribute muss ein Integer sein.',
    'ip'                   => ':attribute muss eine gültige IP-Adresse sein.',
    'json'                 => ':attribute muss ein gültiger JSON String sein.',
    'max'                  => [
        'numeric' => ':attribute darf nicht größer sein als :max.',
        'file'    => ':attribute darf nicht größer sein als :max Kilobyte.',
        'string'  => ':attribute darf höchstens :max Zeichen enthalten.',
        'array'   => ':attribute darf höchstens :max Elemente enthalten.',
    ],
    'mimes'                => ':attribute muss einen dieser Mime-Types haben: :values.',
    'mimetypes'            => ':attribute muss einen dieser Mime-Types haben: :values.',
    'min'                  => [
        'numeric' => ':attribute darf nicht kleiner sein als :min.',
        'file'    => ':attribute darf nicht kleiner sein als :min Kilobyte.',
        'string'  => ':attribute muss mindestens :min Zeichen enthalten.',
        'array'   => ':attribute muss mindestens :min Elemente enthalten.',
    ],
    'not_in'               => 'Auswahl für :attribute ist ungültig.',
    'numeric'              => ':attribute Muss eine Zahl sein.',
    'present'              => ':attribute muss vorhanden sein.',
    'regex'                => 'Format von :attribute ist ungültig.',
    'required'             => ':attribute muss angegeben werden.',
    'required_if'          => ':attribute muss angegeben werden, wenn :other gleich :value ist.',
    'required_unless'      => ':attribute muss angegeben werden, es sei denn :other ist eines von: :values.',
    'required_with'        => ':attribute muss angegeben werden, wenn :values angegeben ist.',
    'required_with_all'    => ':attribute muss angegeben werden, wenn :values angegeben sind.',
    'required_without'     => ':attribute muss angegeben werden, wenn :values nicht angegeben ist.',
    'required_without_all' => ':attribute muss angegeben werden, wenn :values nicht angegeben sind.',
    'same'                 => ':attribute und :other müssen übereinstimmen.',
    'size'                 => [
        'numeric' => ':attribute muss gleich :size sein.',
        'file'    => ':attribute muss :size Kilobyte groß sein.',
        'string'  => ':attribute muss :size Zeichen enthalten.',
        'array'   => ':attribute muss :size Elemente enthalten.',
    ],
    'string'               => ':attribute muss vom Typ String sein.',
    'timezone'             => ':attribute muss eine gültige Zeitzone sein.',
    'unique'               => ':attribute existiert bereits.',
    'uploaded'             => ':attribute konnte nicht hochgeladen werden.',
    'url'                  => 'Das Format von :attribute ist ungültig.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
