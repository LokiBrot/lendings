<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Das Passwort muss aus mindestens sechs Zeichen bestehen und mit der Passwortbestätigung übereinstimmen.',
    'reset' => 'Ihr Passwort wurde zurückgesetzt.',
    'sent' => 'Wir haben Ihnen einen Link zum zurücksetzen des Passworts per Email geschickt.',
    'token' => 'Dieser Passwortwiederherstellungstoken ist ungültig.',
    'user' => "Wir können keinen Nutzer mit dieser Emailadresse finden.",

];
