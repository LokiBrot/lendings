<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Lendings\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\Lendings\Item::class, function (\Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'description' => $faker->text(),
    ];
});

$factory->define(\Lendings\ItemType::class, function (\Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'datafields' => $faker->words(10),
    ];
});

$factory->define(\Lendings\ItemDataField::class, function (\Faker\Generator $faker) {
    $types = collect(['string', 'text', 'int', 'float', 'bool']);

    $type = $types->random();
    $name = $faker->word;
    $value = null;

    switch ($type) {
        case 'string':
            $value = $faker->word;
            break;
        case 'text':
            $type = 'string';
            $value =  $faker->text();
            break;
        case 'int':
            $value = $faker->randomDigit;
            break;
        case 'float':
            $value = $faker->randomFloat();
            break;
        case 'bool':
            $value = $faker->boolean();
            break;
        default:
            // nothing
    }

    return [
        'name' => $name,
        'value' => $value,
        'type' => $type,
    ];
});

