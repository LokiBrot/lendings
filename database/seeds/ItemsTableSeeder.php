<?php

use Illuminate\Database\Seeder;
use Lendings\ItemType;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemTypes = ItemType::all();

        $itemTypes->each(function ($itemType) {
            /** @var ItemType $itemType */
            $itemType->items()->saveMany(factory(\Lendings\Item::class, 10)->make());
        });
    }
}
