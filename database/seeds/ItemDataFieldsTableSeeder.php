<?php

use Illuminate\Database\Seeder;
use Lendings\Item;
use Lendings\ItemDataField;

class ItemDataFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = Item::all();

        $items->each(function ($item) {
            /** @var Item $item */
            $item->_data()->saveMany(factory(ItemDataField::class, 10)->make());
        });
    }
}
